import { ref, computed } from 'vue'
import type { Ref } from 'vue'
import { defineStore } from 'pinia'
import type { IEstudiante } from '@/interfaces/IEstudiante'

export const useStudentStore = defineStore('students', () => {
  const students = ref([]) as Ref<IEstudiante[]>
  let student= ref({}) as Ref<IEstudiante>

  const url = 'http://65e8dab54bb72f0a9c508303.mockapi.io/dev/api/Alumnos' 

  async function getStudent(id:number) {
    try {
        const respuesta = await fetch(`${url}/${id}`, {
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
              },
        })
        const resultado = await respuesta.json()
        console.log(resultado);
        student.value = resultado
        
    } catch (error) {
        console.log(`Error al obtener al estudiante con id: ${id}`, error);
        
    }
  }

  return { student, students, getStudent }
})
