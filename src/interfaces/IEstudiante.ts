export interface IEstudiante {
    id:number
    name:string
    email:string
    group:string
}