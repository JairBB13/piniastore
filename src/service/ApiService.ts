import { ref } from "vue"
import type { Ref } from "vue"
import type { IEstudiante } from "@/interfaces/IEstudiante"
import { useStudentStore } from "@/stores/students"

export default class ApiService{


    private url = 'http://65e8dab54bb72f0a9c508303.mockapi.io/dev/api/Alumnos' 
    private studentStore = useStudentStore()

    constructor(){ }
    
   async getStudents(): Promise<void> {
        try {
            const respuesta = await fetch(this.url, {
                headers: {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                  },
            })
            const resultado = await respuesta.json()
            console.log(resultado);
     
            this.studentStore.students = resultado
        } catch (error) {
            console.error('Error al obtener los usuarios' + error)
        }
   }

   async getStudent(id:number) {
        try {
            const respuesta = await fetch(`${this.url}/${id}`, {
                headers: {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                  },
            })
            const resultado = await respuesta.json()
            console.log(resultado);
            this.studentStore.student = resultado
            
        } catch (error) {
            console.log(`Error al obtener al estudiante con id: ${id}`, error);
            
        }
   }

   async CreateStudent( datos:any ) {
    try {
        await fetch(this.url, {
            method: 'POST',
            body: JSON.stringify({ ...datos }),
            headers: {
                'Content-Type': 'application/json'
              },
            
        })
        .then((response) => console.log("Estudiante agregado", response));

    } catch (error) {
        console.error('Error al crear estudiante' + error)
    }
   }

   async DeleteStudent(id: number) {
        try {
            await fetch(`${this.url}/${id}`, {
                method: 'DELETE',
            })
            .then((response) => console.log("Estudiante eliminado", response));
            
        } catch (error) {
            console.log(`Error al eliminar el estudiante con id: ${id}`, error);
        }

   }

}